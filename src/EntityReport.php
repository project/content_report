<?php
/**
 * @file
 * Contains \EntityReport.
 */

/**
 * Abstract class for building entity reports.
 */
abstract class EntityReport {
  /**
   * The machine_name of the report.
   *
   * @var string
   */
  protected $name;

  /**
   * The type of entity we're reporting on.
   *
   * @var string
   */
  protected $entityType;

  /**
   * The base table for data lookup.
   *
   * Ex. For Nodes it is 'node'.
   *
   * @var string
   */
  protected $baseTable;

  /**
   * The field to index the table rows.
   *
   * Ex. For Nodes it is 'type'.
   *
   * @var string
   */
  protected $index;

  /**
   * The HTML table headers for the report data.
   *
   * @var array
   */
  protected $headers = array();

  /**
   * The data for the report table.
   *
   * @var array
   */
  protected $data = array();

  /**
   * A list of footnotes to render.
   *
   * @var array
   */
  protected $footnotes = array();

  /**
   * Make the table sortable.
   *
   * @var bool
   */
  public $sortable = TRUE;

  /**
   * The date format to use on timestamp fields.
   *
   * @var string
   */
  public $dateFormat = 'Y-m-d';

  /**
   * EntityReport constructor.
   *
   * @param string $name
   *   The machine_name of the report.
   * @param string $entity_type
   *   The type of entity we're reporting on.
   * @param string $index
   *   The field to index data on.
   * @param string $title
   *   The optional title of the report for display.  Should be wrapped in t().
   */
  public function __construct($entity_type, $index, $name, $title = NULL) {
    $info = entity_get_info($entity_type);

    $this->entityType = $entity_type;
    $this->baseTable = $info['base table'];
    $this->index = $index;
    $this->name = $name;
    $this->title = $title;

    $this->buildInitialData();
  }

  /**
   * Set the initial report data using the index field.
   */
  protected function buildInitialData() {
    $query = $this->baseDbSelect();
    $data = $this->getDataFromQuery($query);

    // Create the first column of data from the index field.
    foreach ($data as $row) {
      $index = $row->{$this->index};
      $this->data[$index][$this->index] = $index;
    }
  }

  /**
   * Get a base query for extracting summary data from a table.
   *
   * @return \SelectQuery
   *   A query object.
   */
  public function baseDbSelect() {
    $query = db_select($this->baseTable, $this->baseTable);
    $query->addField($this->baseTable, $this->index);
    $query->groupBy("{$this->baseTable}.{$this->index}");

    return clone($query);
  }

  /**
   * Get the name of the report.
   *
   * @return string
   *   The report name.
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Get the report entity type.
   *
   * @return string
   *   The entity type.
   */
  public function getEntityType() {
    return $this->entityType;
  }

  /**
   * Get the header data for the table.
   *
   * @return array
   *   The formatted header array.
   *
   * @see theme_table()
   */
  public function getHeaders() {
    // Append footnotes.
    foreach ($this->footnotes as $id => $note) {
      if (isset($this->headers[$note['col']])) {
        $this->headers[$note['col']]['data'] .= theme('content_report_footnote_source', array(
          'note_id' => $id,
          'note' => $note,
        ));
      }
    }

    return $this->headers;
  }

  /**
   * Get the data for the table.
   *
   * @return array
   *   The formatted data.
   *
   * @see theme_table()
   */
  public function getData() {
    return $this->data;
  }

  /**
   * Extract the data from a query.
   *
   * @param \SelectQuery $query
   *   A DB Select query.
   *
   * @return array|null
   *   The results of the query.
   */
  public function getDataFromQuery(SelectQuery $query) {
    return $query->execute()->fetchAll();
  }

  /**
   * Add a header column.
   *
   * @param string $column
   *   The column machine_name.
   * @param string $data
   *   The translated column title.
   * @param mixed $default
   *   A default value to populate data fields for this column.
   *
   * @return $this
   *   This object for chaining.
   */
  public function addHeader($column, $data, $default = NULL) {
    // Add the header.
    $this->headers[$column] = array('data' => $data);

    // Set default values in the data so data columns match header columns and
    // there are no empty cells.
    if (!is_null($default)) {
      foreach ($this->data as &$row_data) {
        $row_data[$column] = $default;
      }
    }

    return $this;
  }

  /**
   * Add data to the table.
   *
   * @param string $index
   *   The row index.
   * @param string $column
   *   The column.
   * @param mixed $datum
   *   The data value.
   *
   * @return $this
   *   This object for chaining.
   */
  public function addData($index, $column, $datum) {
    $this->data[$index][$column] = $datum;

    return $this;
  }

  /**
   * Add a footnote to a table header.
   *
   * @param string $col
   *   The column to add the footnote to.
   * @param string $note
   *   The note.
   */
  public function addFootnote($col, $note) {
    static $i = 1;

    $id = "{$this->name}-{$i}";
    $this->footnotes[$id] = array(
      'i' => $i,
      'col' => $col,
      'note' => $note,
    );

    $i++;
  }

  /**
   * Create the render array for the table.
   *
   * @return array
   *   The render array.
   */
  public function build() {
    $module = ContentReportModule::load();
    $build = array();

    // Build the table.
    $build[$this->name]['table'] = array(
      '#theme' => 'table',
      '#caption' => filter_xss($this->title),
      '#header' => $this->getHeaders(),
      '#rows' => $this->getData(),
      '#sticky' => TRUE,
    );

    // Build the footnotes.
    $build[$this->name]['footnotes'] = array();
    foreach ($this->footnotes as $id => $footnote) {
      $build['overview']['footnotes'][] = array(
        '#theme' => 'content_report_footnote_target',
        '#note_id' => $id,
        '#note' => $footnote,
      );
    }

    // Add the tablesorter libraries.
    if ($this->sortable) {
      $module->attachExternalLibrary($build[$this->name]['table'], 'jquery.tablesorter');
      $module->attachLibrary($build[$this->name]['table'], 'content_report_table_sort');

      // Add the class to the table.
      $build[$this->name]['table']['#attributes']['class'][] = 'sortable';
    }

    return $build;
  }

  /**
   * Format a timestamp into a readable date.
   *
   * @param int $timestamp
   *   A unix timestamp.
   *
   * @return string
   *   The timestamp formatted to $this->dateFormat.
   */
  protected function formatDate($timestamp) {
    return format_date($timestamp, 'custom', $this->dateFormat);
  }

}
