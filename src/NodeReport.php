<?php
/**
 * @file
 * Contains \NodeReport.
 */

/**
 * Build a node report.
 */
class NodeReport extends EntityReport {
  /**
   * NodeReport constructor.
   *
   * @param string $name
   *   The report machine name.
   */
  public function __construct($name, $title) {
    parent::__construct('node', 'type', $name, $title);

    // Add the base header.
    $this->addHeader('type', t("Type"));
  }

  /**
   * Add a total count column to the table.
   */
  public function addTotalCount() {
    $this->addHeader('total', t("Total"), 0);

    // Create the default data.
    $query = $this->baseDbSelect();
    $query->addExpression('COUNT(*)', 'total');
    $data = $this->getDataFromQuery($query);

    // Add the total count data to the table.
    foreach ($data as $row) {
      $index = $row->{$this->index};
      $this->addData($index, 'total', $row->total);
    }
  }

  /**
   * Add columns for published and unpublished counts.
   */
  public function addStatusCounts() {
    $this->addHeader('published', t("Published"), 0)
         ->addHeader('unpublished', t("Unpublished"), 0);

    // Create the default data.
    $query = $this->baseDbSelect();
    $query->addExpression('COUNT(*)', 'total');
    $query->addField($this->baseTable, 'status');
    $query->groupBy('status');
    $data = $this->getDataFromQuery($query);

    // Add the total count data to the table.
    foreach ($data as $row) {
      $index = $row->{$this->index};
      $col = $row->status === "0" ? 'unpublished' : 'published';
      $this->addData($index, $col, $row->total);
    }
  }

  /**
   * Add columns for timestamps.
   */
  public function addTimeStamps() {
    $this->addHeader('oldest', t("Oldest"), 0);
    $this->addHeader('newest', t("Newest"), 0);
    $this->addHeader('last_modified', t("Last Modified"), 0);

    // Create the default data.
    $query = $this->baseDbSelect();
    $query->addExpression('MAX(created)', 'newest');
    $query->addExpression('MIN(created)', 'oldest');
    $query->addExpression('MAX(changed)', 'last_modified');
    $data = $this->getDataFromQuery($query);

    // Add the total count data to the table.
    foreach ($data as $row) {
      $index = $row->{$this->index};
      $this->addData($index, 'oldest', $this->formatDate($row->oldest))
           ->addData($index, 'newest', $this->formatDate($row->newest))
           ->addData($index, 'last_modified', $this->formatDate($row->last_modified));
    }
  }

}
