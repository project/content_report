<?php
/**
 * Template: report-footnotes.tpl.php
 *
 * Render the footnotes below a report.
 *
 * @variables
 *   - string $note_id
 *     The ID for the footnote, consisting of the report name and footnote
 *     number.
 *   - array $note
 *     The note properties.
 *     - $note['i']: The footnote number.
 *     - $note['note']: The footnote text.
 *     - $note['col']: The column the footnote is attached to.
 */
?>
<sup class="footnote-source">
  <a href="#<?php print $note_id; ?>" id="<?php print $note_id; ?>-ref"><?php print $note['i']; ?></a>
</sup>
