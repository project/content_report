<?php
/**
 * Template: footnote-target.tpl.php
 *
 * Render the target of the footnote reference below the table.
 *
 * @variables
 *   - array $footnote
 *     The footnote properties.
 *     - $footnote['#note_id']: The footnote ID.
 *     - $footnote['#note']['i']: The footnote number.
 *     - $footnote['#note']['note']: The footnote text.
 *     - $footnote['#note']['col']: The column the footnote is attached to.
 */
?>
<sup id="<?php print $footnote['#note_id']; ?>" class="footnote-target">
  <?php print $footnote['#note']['i']; ?>. <?php print $footnote['#note']['note']; ?>
</sup>
