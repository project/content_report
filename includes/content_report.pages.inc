<?php

/**
 * @file
 * Page callbacks.
 */

/**
 * Render the content type overview page.
 *
 * @return array
 *   A render array for a table.
 */
function content_report_overview_page() {
  $report = new NodeReport('overview', t("Content Overview Report"));

  // Add the necessary headers first for sorting.
  $report->addTotalCount();
  $report->addStatusCounts();
  $report->addTimeStamps();

  // Allow modules to alter the overview report data.
  drupal_alter('content_report', $report);

  // Construct the table array.
  return $report->build();
}
