(function ($) {
  "use strict";

  /**
   * Implements the jQuery table sorter plugin.
   */
  Drupal.behaviors.contentReportTableSort = {
    /**
     * Attach the behavior to Drupal.
     */
    attach: function (context, settings) {
      if ($.isFunction($.fn.tablesorter)) {
        $(context).find("table.sortable").tablesorter();
      }
    }
  };
})(jQuery);
